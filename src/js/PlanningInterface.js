import { store } from "./store.js";
import ROSInterface from "./ROSInterface.js";

export default class PlanningInterface {
  constructor(emitter) {
    this.iface = new ROSInterface();
    this.plan_msg = "";
    this.plan = [];
    this.subscribePlan();
    this.emitter = emitter;
    this.colors = {
      r: "red",
      g: "green",
      b: "blue",
      y: "yellow",
      w: "white",
      l: "light_green",
      o: "olive",
    };
  }

  rosPlanIface = (msg) => {
    if (this.plan_msg.localeCompare(msg) != 0) {
      this.plan_msg = msg;
      // Reset Plan
      // store.plan.splice(0, store.plan.length);
      store.plan.length = 0;
      const plan = JSON.parse(msg);
      console.log("New Plan Received", plan)
      /**
       * Create Actions
       */
      this.robot_acts_cnt = 0;
      if (plan.length !== 0) {
        for (let lego in plan) {
          // console.log("action", lego, plan[lego]);
          let action = {};
          action.color = this.colors[lego[0]];
          action.size = 2;
          if (lego[1] == "b") {
            action.size = 4;
          }
          action.changed = false;
          action.lego = lego;
          action.act_id = plan[lego].id;
          action.actor = plan[lego].actor;

          let p_xx_yy_z = plan[lego].pick_.split("_");
          action.pick = {};
          action.pick.x = Number(p_xx_yy_z[1]);
          action.pick.y = Number(p_xx_yy_z[2]);
          action.pick.z = Number(p_xx_yy_z[3]);

          let pp_xx_yy_z = plan[lego].place_.split("_");
          action.place = {};
          action.place.x = Number(pp_xx_yy_z[1]);
          action.place.y = Number(pp_xx_yy_z[2]);
          action.place.z = Number(pp_xx_yy_z[3]);

          action.feasible = true;

          action.status = plan[lego].status;
          if (action.actor == "robot_arm" && action.status != "executed") {
            this.robot_acts_cnt += 1;
          }
          if (action.status !== "pending") {
            action.changed = true;
          }
          if (plan[lego].status === "current") {
            let place_pos = action.place;
            // let rot_ = plan[lego].rotation;
            let rot_ = false;
            if (JSON.stringify(place_pos) === JSON.stringify({ x: 3, y: 11, z: 0 })) {
              rot_ = true;
            }
            this.emitter.emit("action-execution", { lego, place_pos, rot_ });
            const log = {
              time_stamp: new Date().toISOString(),
              message: "Action P&P [" + lego + "] Was executed by the Robot!",
              color: "white",
              name: store.logs.length,
            };
            store.logs.push(log);
          }
          this.plan.push(action);
        }
      }
      if (this.robot_acts_cnt > 0) {
        store.empty_plan = false;
      }
      else { 
        store.empty_plan = true;
      }

      store.plan = this.plan;
      // console.log("Plan saved:", store.plan)
      this.emitter.emit('render-plan');
    }
  };

  subscribePlan = () => {
    this.iface.subscriber(
      "/accumulated_plan",
      "std_msgs/String",
      this.rosPlanIface
    );
  };

  getPlan = () => {
    return this.plan;
  };
}